﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamWorkGH.ServiceReference;

namespace TeamWorkGH
{
    /// <summary>
    /// Marketplace.xaml 的交互逻辑
    /// </summary>
    public partial class Marketplace : Window
    {
        UserDTO user;
        GitHubServiceClient client;
        public Marketplace(string userName)
        {
            InitializeComponent();

            client = new GitHubServiceClient();
            this.user = client.FindUserByUserName(userName);
            this.UserName.Text = user.UserName;

            ProjectList.Items.Add("UserName/ProjectName\tProjectId\tComment");
            try
            {
                foreach (var item in client.GetAllPublicProjects())
                {
                    ProjectList.Items.Add(item.Owner + '/' + item.ProjectName + '\t' + item.ProjectId.ToString() + '\t' + item.Comment);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
         }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow mainWindow = new MainWindow(user.UserName);
            mainWindow.Show();
            this.Close();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
            {
                Login login = new Login();
                int itemName = comboBox1.SelectedIndex;
                switch (itemName)
                {
                    case 1: login.Show(); this.Close(); break;
                    default:
                        break;
                }
            }
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void ProjectList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string[] s = ProjectList.SelectedItem.ToString().Split('\t');
            if (s[1] != "ProjectId")
            {
                ProjectDTO project = client.FindProjectById(Convert.ToInt32(s[1]));
                ProjectWindow projectWindow = new ProjectWindow(user.UserName, project.ProjectId);
                projectWindow.Show();
                this.Close();
            }
        }
    }
}
