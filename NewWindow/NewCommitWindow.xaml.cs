﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TeamWorkGH.ServiceReference;
using TeamWorkGH.Utils;

namespace TeamWorkGH.NewWindow
{
    /// <summary>
    /// NewCommitWindow.xaml 的交互逻辑
    /// </summary>
    public partial class NewCommitWindow : Window
    {
        private bool flag = false;
        ProjectDTO project;
        string UserName;
        GitHubServiceClient client;
        public NewCommitWindow(string UserName, int projectId, ProjectWindow projectWindow)
        {
            InitializeComponent();

            client = new GitHubServiceClient();
            this.project = client.FindProjectById(projectId);
            this.UserName = UserName;
            this.UserNameLabel.Content += UserName;
            this.Owner = projectWindow;
        }

        private void SelectFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            if (!(bool)openFile.ShowDialog())
            {
                MessageBox.Show("选择文件不能为空");
            }
            else if (Path.GetExtension(openFile.FileName)!=".zip")
            {
                MessageBox.Show("目前只支持zip文件");
            }
            else
            {
                PathTextbox.Text = openFile.FileName;
            }
        }

        private void Commit_Click(object sender, RoutedEventArgs e)
        {
            if (CommentTextbox.Text == "")
            {
                MessageBox.Show("CommitComment不能为空");
            }
            else
            {
                MessageBox.Show(Path.GetFileName(PathTextbox.Text));
                client.AddCommitByProjectId(CommentTextbox.Text, project.ProjectId, UserName, 
                    Path.GetFileName(PathTextbox.Text), ref flag, UpAndDownloadFiles.UploadStream(PathTextbox.Text));

                MessageBox.Show("上传成功");
                this.Close();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Owner.Activate();
        }
    }
}
