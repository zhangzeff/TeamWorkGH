﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TeamWorkGH.ServiceReference;
using TeamWorkGH.Utils;

namespace TeamWorkGH.NewWindow
{
    /// <summary>
    /// NewRequestWindow.xaml 的交互逻辑
    /// </summary>
    public partial class NewRequestWindow : Window
    {
        private bool flag = true;
        ProjectDTO project;
        string UserName;
        GitHubServiceClient client;
        public NewRequestWindow(string UserName, int projectId, ProjectWindow projectWindow)
        {
            InitializeComponent();

            client = new GitHubServiceClient();
            this.project = client.FindProjectById(projectId);
            this.UserName = UserName;
            this.Owner = projectWindow;

            this.UserNameLabel.Content += UserName;
        }

        private void SelectBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            if (!(bool)openFile.ShowDialog())
            {
                MessageBox.Show("选择文件不能为空");
            }
            else if (Path.GetExtension(openFile.FileName) != ".zip")
            {
                MessageBox.Show("目前只支持zip文件");
            }
            else
            {
                PathTextbox.Text = openFile.FileName;
            }
        }

        private void CommitBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CommentTextbox.Text == "")
            {
                MessageBox.Show("CommitComment不能为空");
            }
            else
            {
                client.AddRequestByProjectId(CommentTextbox.Text, project.ProjectId, UserName,
                    Path.GetFileNameWithoutExtension(PathTextbox.Text), ref flag, UpAndDownloadFiles.UploadStream(PathTextbox.Text));
                MessageBox.Show("上传成功");
                this.Close();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Owner.Activate();
        }
    }
}
