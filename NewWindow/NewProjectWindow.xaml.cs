﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamWorkGH.ServiceReference;
namespace TeamWorkGH
{
    /// <summary>
    /// NewProjectWindow.xaml 的交互逻辑
    /// </summary>
    public partial class NewProjectWindow : Window
    {
        GitHubServiceClient client;
        UserDTO user;
        public NewProjectWindow(string userName, MainWindow mainWindow)
        {
            InitializeComponent();

            client = new GitHubServiceClient();
            this.user = client.FindUserByUserName(userName);
            uName.Content += user.UserName;
            this.Owner = mainWindow;
        }

        private void ret_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow(user.UserName);
            window.Show();
            this.Close();
        }

        private void NewProject_Click(object sender, RoutedEventArgs e)
        {
            if (ProjectName.Text.Length<3 || ProjectName.Text.Length>10)
            {
                MessageBox.Show("项目名长度至少为3，至多为10。");
            }
            else if (ProjectComment.Text=="")
            {
                MessageBox.Show("项目描述不能为空");
            }
            else
            {
                client.AddProjectByUserName(ProjectName.Text, user.UserName, ProjectComment.Text, (bool)IsPrivate.IsChecked);
                MessageBox.Show("项目创建成功");
                this.Close();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.Owner.Activate();
        }
    }
}
