﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using TeamWorkGH.ServiceReference;

namespace TeamWorkGH
{
    /// <summary>
    /// Commits.xaml 的交互逻辑
    /// </summary>
    public partial class Commits : Window
    {
        ProjectDTO project;
        UserDTO user;
        GitHubServiceClient client;
        public Commits(int projectId, string userName)
        {
            InitializeComponent();

            client = new GitHubServiceClient();
            this.project = client.FindProjectById(projectId);
            this.user = client.FindUserByUserName(userName);
            this.UserName.Text = userName;
            


            CommitList.Items.Add("Select\tCommitId\tUserName\tTime\tComment");
            foreach (var item in client.GetAllCommitsByProjectId(project.ProjectId))
            {
                if (item.CommitUrl==project.CurrentUrl)
                {
                    CommitList.Items.Add("√" + '\t' + item.CommitId + '\t' + user.UserName + '\t' + item.CommitTime.ToString() + '\t' + item.Comment);
                }
                else
                {
                    CommitList.Items.Add(" " + '\t' + item.CommitId + '\t' + user.UserName + '\t' + item.CommitTime.ToString() + '\t' + item.Comment);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
            {
                Login login = new Login();
                int itemName = comboBox1.SelectedIndex;
                switch (itemName)
                {
                    case 1: login.Show(); this.Close(); break; ;
                    default:
                        break;
                }
            }
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ProjectWindow projectWindow = new ProjectWindow(user.UserName, project.ProjectId);
            projectWindow.Show();
            this.Close();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void TextBlock_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            Marketplace marketplace = new Marketplace(user.UserName);
            marketplace.Show();
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            ProjectWindow projectWindow = new ProjectWindow(user.UserName, project.ProjectId);
            projectWindow.Show();
            this.Close();
        }

        private void CommitList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string[] s = CommitList.SelectedItem.ToString().Split('\t');
            if (s[0]==" ")
            {
                if (MessageBox.Show("想要CallBack吗？", "CallBack", MessageBoxButton.YesNo)==MessageBoxResult.Yes)
                {
                    client.ModifyCurrentUrlById(Convert.ToInt32(s[1]), project.ProjectId, user.UserName);
                    MessageBox.Show("Complete!");
                    ProjectWindow projectWindow = new ProjectWindow(user.UserName, project.ProjectId);
                    projectWindow.Show();
                    this.Close();
                }
            }
        }
    }
}
