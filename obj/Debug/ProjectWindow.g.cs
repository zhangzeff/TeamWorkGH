﻿#pragma checksum "..\..\ProjectWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "C5ACC9F17D47504F3004ECD2E683FB196F1AA5A49136E5083EE01CB92AE4A22D"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using TeamWorkGH;


namespace TeamWorkGH {
    
    
    /// <summary>
    /// ProjectWindow
    /// </summary>
    public partial class ProjectWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock UserName;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox comboBox1;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NameAndProjectName;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CommitBtn;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CommitMsg;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PullrequestBtn;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock PullRequestMsg;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Fork;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button UpRequest;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button UpCommit;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button down;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox commentTextbox;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addComment;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox CommentList;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\ProjectWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TreeView tree;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TeamWorkGH;component/projectwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ProjectWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\ProjectWindow.xaml"
            ((TeamWorkGH.ProjectWindow)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Window_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 15 "..\..\ProjectWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 16 "..\..\ProjectWindow.xaml"
            ((System.Windows.Controls.TextBlock)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.TextBlock_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.UserName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            
            #line 18 "..\..\ProjectWindow.xaml"
            ((System.Windows.Controls.TextBlock)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.TextBlock_MouseLeftButtonDown_2);
            
            #line default
            #line hidden
            return;
            case 6:
            this.comboBox1 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 19 "..\..\ProjectWindow.xaml"
            this.comboBox1.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ComboBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.NameAndProjectName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.CommitBtn = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\ProjectWindow.xaml"
            this.CommitBtn.Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            return;
            case 9:
            this.CommitMsg = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.PullrequestBtn = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\ProjectWindow.xaml"
            this.PullrequestBtn.Click += new System.Windows.RoutedEventHandler(this.Button_Click_2);
            
            #line default
            #line hidden
            return;
            case 11:
            this.PullRequestMsg = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.Fork = ((System.Windows.Controls.Button)(target));
            
            #line 52 "..\..\ProjectWindow.xaml"
            this.Fork.Click += new System.Windows.RoutedEventHandler(this.Fork_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.UpRequest = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\ProjectWindow.xaml"
            this.UpRequest.Click += new System.Windows.RoutedEventHandler(this.UpRequest_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.UpCommit = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\ProjectWindow.xaml"
            this.UpCommit.Click += new System.Windows.RoutedEventHandler(this.UpCommit_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.down = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\ProjectWindow.xaml"
            this.down.Click += new System.Windows.RoutedEventHandler(this.down_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.commentTextbox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.addComment = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\ProjectWindow.xaml"
            this.addComment.Click += new System.Windows.RoutedEventHandler(this.addComment_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.CommentList = ((System.Windows.Controls.ListBox)(target));
            return;
            case 19:
            this.tree = ((System.Windows.Controls.TreeView)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

