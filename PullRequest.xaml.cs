﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamWorkGH.ServiceReference;

namespace TeamWorkGH
{
    /// <summary>
    /// PullRequest.xaml 的交互逻辑
    /// </summary>
    public partial class PullRequest : Window
    {
        UserDTO user;
        ProjectDTO project;
        GitHubServiceClient client;
        public PullRequest(string userName, int projectId)
        {
            InitializeComponent();

            this.client = new GitHubServiceClient();
            this.user = client.FindUserByUserName(userName);
            this.project = client.FindProjectById(projectId);
            this.UserName.Text = userName;
            

            RequestList.Items.Add("RequestId\tUserName\tTime\tComment");
            foreach (var item in client.GetAllRequestsByProjectId(project.ProjectId))
            {
                RequestList.Items.Add(item.RequestId.ToString() + '\t' + item.UserName + '\t' + item.RequestTime.ToString() + '\t' + item.Comment);
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count > 0)
            {
                Login login = new Login();
                int itemName = comboBox1.SelectedIndex;
                switch (itemName)
                {
                    case 1: login.Show(); this.Close(); break;
                    default:
                        break;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Marketplace marketplace = new Marketplace(user.UserName);
            marketplace.Show();
            this.Close();
        }

        private void RequestList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string[] s = RequestList.SelectedItem.ToString().Split('\t');
            if (s[0]!="RequestId" && user.UserName==project.Owner)
            {
                MessageBoxResult result = MessageBox.Show("Yes is Accept it.\nNo is reject it.", "Are you Ok?", MessageBoxButton.YesNoCancel);
                if (result == MessageBoxResult.Yes)
                {
                    client.AcceptPullRequestById(project.ProjectId, Convert.ToInt32(s[0]), user.UserName);
                    MessageBox.Show("Accept it!");
                    ProjectWindow projectWindow = new ProjectWindow(user.UserName, project.ProjectId);
                    projectWindow.Show();
                    this.Close();
                }
                else if (result == MessageBoxResult.No)
                {
                    client.RejectRequestById(project.ProjectId, Convert.ToInt32(s[0]), user.UserName);
                    MessageBox.Show("Reject it!");
                    ProjectWindow projectWindow = new ProjectWindow(user.UserName, project.ProjectId);
                    projectWindow.Show();
                    this.Close();
                }
            }
            
        }
    }
}
